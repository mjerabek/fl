#include "fl/optional.hpp"

#include "catch.hpp"

struct Nontrivial {
    Nontrivial()
    {
    }
    ~Nontrivial()
    {
    }
};

TEST_CASE("basics")
{
    fl::optional<int> a;
    REQUIRE_FALSE(a.valid());
    a = 42;
    REQUIRE(a.valid());
    REQUIRE(*a == 42);

    fl::optional<int> b = 16;
    REQUIRE(b.valid());
    REQUIRE(*b == 16);

    b = a;
    REQUIRE(b.valid());
    REQUIRE(*b == 42);

    a.reset();
    REQUIRE_FALSE(a.valid());
}

TEST_CASE("nullopt") {
    SECTION("constructor") {
        fl::optional<int> a(fl::nullopt);
        fl::optional<Nontrivial> b(fl::nullopt);
        CHECK_FALSE(a.valid());
        CHECK_FALSE(b.valid());
    }
    SECTION("assignment") {
        fl::optional<int> a(42);
        fl::optional<Nontrivial> b(Nontrivial{});
        a = fl::nullopt;
        CHECK_FALSE(a.valid());
        b = fl::nullopt;
        CHECK_FALSE(b.valid());
    }
}

namespace {
    template <typename T>
    fl::optional<std::remove_reference_t<T>> make_optional(T &&t) {
        return {std::forward<T>(t)};
    }
}

TEST_CASE("monadic") {
    SECTION("valid to valid") {
        fl::optional<int> res = make_optional(42) >>= [](int a) {
            REQUIRE(a == 42);
            return make_optional(12);
        };
        REQUIRE(res.valid());
        REQUIRE(*res == 12);
    }

    SECTION("valid to empty") {
        const auto a = make_optional(42);
        fl::optional<int> res = a >>= [](int const &a) {
            REQUIRE(a == 42);
            return fl::optional<int>();
        };
        REQUIRE_FALSE(res.valid());
    }

    SECTION("empty to empty") {
        fl::optional<int> res = fl::optional<int>() >>= [](int) {
            FAIL("should not be called");
            return fl::optional<int>();
        };
        REQUIRE_FALSE(res.valid());
    }

}
