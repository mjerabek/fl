"""
GDB module to hide some stack frames in backtrace.
Hidden frames:
- implementation of stdlib functions: std::_*
- fl (functional library) innards: fl::*

Usage:
gdb> source gdb_filter.py
gdb> bt
"""

import gdb
import re

class Filter(object):
    def __init__(self):
        self.name = "colorize"
        # Give this a high priority so it runs soon.
        self.priority = 50
        self.enabled = True
        gdb.frame_filters[self.name] = self

    def include_frame(self, frame):
        f = frame.function()
        if f.startswith('std::_'):
            return False
        elif re.match(r'^fl::.*', f):
            return False
        return True

    def filter(self, frame_iter):
        return filter(self.include_frame, frame_iter)

Filter()
