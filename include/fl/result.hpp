#pragma once
#include "variant.hpp"
#include "optional.hpp"
#include <system_error>
#include <cerrno>
#include <cassert>

namespace fl {

template <typename T>
struct is_result;

template <typename T, typename Terror = std::system_error>
struct Result : public fl::variant<Terror, T>
{
    using error_type = Terror;
    using value_type = T;
    using base = fl::variant<error_type, value_type>;
    using base::base;

    bool valid() const {return base::template is<T>();}
    fl::optional<T> to_optional() && {
        return std::move(*this).match([](error_type &&) {return fl::optional<T>();},
                                      [](T &&v) {return fl::optional<T>(std::move(v));}
                                  );
    }
    T&       operator*() &       { assert(valid()); return base::template get_unchecked<T>(); }
    const T& operator*() const & { assert(valid()); return base::template get_unchecked<T>(); }
    T&&      operator*() &&      { assert(valid()); return std::move(*this).base::template get_unchecked<T>(); }

    T*       operator->()        { return &**this; }
    const T* operator->() const  { return &**this; }

    error_type&       error() &       { assert(!valid()); return base::template get_unchecked<error_type>(); }
    error_type const& error() const & { assert(!valid()); return base::template get_unchecked<error_type>(); }
    error_type&&      error() &&      { assert(!valid()); return std::move(*this).base::template get_unchecked<error_type>(); }

    T unwrap() {
        return std::move(*this).match(
            [](Terror &&e) -> T {
                throw std::move(e);
            },
            [](T &&v) -> T {
                return std::move(v);
            }
        );
    }
};

template <typename T>
struct is_result : std::false_type {};
template <typename T, typename TE>
struct is_result<Result<T, TE>> : std::true_type {};

template <typename T>
struct is_result_cvr : is_result<std::remove_cv_t<std::remove_reference_t<T>>> {};

//! monadic bind
//! (>>=) :: Result a -> (a -> Result b) -> Result b
template <typename T, typename TE, typename F>
static inline auto operator>>=(Result<T, TE> &r, F &&f) {
    using res_type = decltype(f(std::declval<T&>()));
    using error_type = typename Result<T, TE>::error_type;
    static_assert(is_result<res_type>::value, "f must return Result<T>");
    return r.match([](error_type const &s) {return res_type(s);},
                   [&f](T &v) {return f(v);}
    );
}
template <typename T, typename TE, typename F>
static inline auto operator>>=(Result<T, TE> const &r, F &&f) {
    using res_type = decltype(f(std::declval<const T&>()));
    using error_type = typename Result<T, TE>::error_type;
    static_assert(is_result<res_type>::value, "f must return Result<T>");
    return r.match([](error_type const &s) {return res_type(s);},
                   [&f](const T &v) {return f(v);}
    );
}
template <typename T, typename TE, typename F>
static inline auto operator>>=(Result<T, TE> &&r, F &&f) {
    using res_type = decltype(f(std::declval<T&&>()));
    using error_type = typename Result<T, TE>::error_type;
    static_assert(is_result<res_type>::value, "f must return Result<T>");
    return std::move(r).match([](error_type &&s) {return res_type(std::move(s));},
                              [&f](T &&v) {return f(std::move(v));}
    );
}

//! fmap
//! <$> :: Result a -> (a -> b) -> Result b
template <typename R, typename F, typename = std::enable_if_t<is_result_cvr<R>::value>>
static inline auto operator%(R &&r, F &&f) {
    using T = typename std::remove_reference_t<R>::value_type;
    using res_type = std::decay_t<decltype(f(std::declval<T&&>()))>;
    using error_type = typename std::remove_reference_t<R>::error_type;
    return std::forward<R>(r) >>= [&f](auto &&v) {
        return Result<res_type, error_type>(f(std::forward<decltype(v)>(v)));
    };
}

//! templated to enable multiple definitions but keep only one
//! inline -> would be inlined (undesirable)
//! static -> would be duplicated across all compile units
//! inline [[gnu::noinline]] -> possible, but convoluted
//! [[gnu::weak]] -> basically the same as this, but not portable
template <typename = void>
std::system_error make_stdc_error(int _errno, std::string msg) {
    std::error_code ec(_errno, std::generic_category());
    return std::system_error(std::move(ec), std::move(msg));
}

template <typename = void>
std::system_error make_stdc_error(std::string msg) {
    return make_stdc_error(errno, std::move(msg));
}

} // namespace fl
