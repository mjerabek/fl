#pragma once

#include <cassert>
#include <type_traits>
#include <utility>


namespace fl {

namespace detail {
    template<typename... Ts> struct make_void { typedef void type;};
    template<typename... Ts> using void_t = typename make_void<Ts...>::type;
}

struct nullopt_t {};
constexpr nullopt_t nullopt {};

struct empty_t {};

template <typename T, typename = void>
class trivial_optional_storage
{
public:
    constexpr trivial_optional_storage() : m_valid(false), empty() {}
    constexpr trivial_optional_storage(nullopt_t) : m_valid(false), empty() {}
    constexpr trivial_optional_storage(T v) : m_valid(true), value(std::move(v)) {}
    constexpr bool valid() const {return m_valid;}
    constexpr void reset() {m_valid = false;}
protected:
    bool m_valid;
    union {
        empty_t empty;
        T value;
    };
};

template <typename T>
struct optional_traits
{
    // static constexpr T invalid_value = ...;
    static constexpr bool is_valid(const T&) {return true;}
};

template <typename T>
struct optional_traits<const T> : optional_traits<T> {};

template <typename T>
struct optional_traits<volatile T> : optional_traits<T> {};

template <typename T>
struct optional_traits<T*>
{
    static constexpr T* invalid_value = nullptr;
    static constexpr bool is_valid(const T *v) {return v != nullptr;}
};

template <typename T, typename = void>
struct has_invalid_value : std::false_type {};

template <typename T>
struct has_invalid_value<T, detail::void_t<decltype(optional_traits<T>::invalid_value)>> : std::true_type {};

template <typename T>
class trivial_optional_storage<T, detail::void_t<decltype(optional_traits<T>::invalid_value)>>
{
    using traits = optional_traits<T>;
    static constexpr T invalid_value = traits::invalid_value;
public:
    constexpr trivial_optional_storage(nullopt_t) : value(invalid_value) {}
    constexpr trivial_optional_storage() : value(invalid_value) {}
    constexpr trivial_optional_storage(T v) : value(std::move(v)) {}
    constexpr bool valid() const {return traits::is_valid(value);}
    constexpr void reset() {value = invalid_value;}
protected:
    T value;
};


template <typename T>
class nontrivial_optional_storage
{
public:
    ~nontrivial_optional_storage() {
        if (valid())
            value.~T();
    }

    void reset() {
        if (valid())
            value.~T();
        m_valid = false;
    }

    nontrivial_optional_storage(T &&t) noexcept(std::is_nothrow_move_constructible<T>::value)
        : m_valid(true)
        , value(std::move(t))
    {
    }

    nontrivial_optional_storage(const T &t) noexcept(std::is_nothrow_copy_constructible<T>::value)
        : m_valid(true)
        , value(t)
    {
    }

    nontrivial_optional_storage<T>& operator=(T const &t) noexcept(noexcept(std::declval<nontrivial_optional_storage<T>>().copy_assign(t)))            {copy_assign(t); return *this;}
    nontrivial_optional_storage<T>& operator=(T &&t)      noexcept(noexcept(std::declval<nontrivial_optional_storage<T>>().move_assign(std::move(t)))) {move_assign(std::move(t)); return *this;}

    nontrivial_optional_storage<T>& operator=(nontrivial_optional_storage<T> const &o)
        noexcept(std::is_nothrow_assignable<nontrivial_optional_storage<T>, T const&>::value)
    {
        if (!o.valid())
            reset();
        else
            *this = o.value;
        return *this;
    }

    nontrivial_optional_storage<T>& operator=(nontrivial_optional_storage<T> &&o)
        noexcept(std::is_nothrow_assignable<nontrivial_optional_storage<T>, T &&>::value)
    {
        if (!o.valid())
            reset();
        else
            *this = o.value;
        return *this;
    }

    constexpr nontrivial_optional_storage() : empty(), m_valid(false) {}
    constexpr nontrivial_optional_storage(nullopt_t) : empty(), m_valid(false) {}
    constexpr bool valid() const {return m_valid;}
private:
    // copy assignment if T is copy assignable
    template <typename TT = T>
    void copy_assign(T const &t, std::enable_if_t<std::is_copy_assignable<TT>::value>* = nullptr)
        noexcept(std::is_nothrow_copy_constructible<T>::value && std::is_nothrow_copy_assignable<T>::value)
    {
        if (valid()) {
            value = t;
        } else {
            new (&value) T(t);
            m_valid = true;
        }
    }

    // copy assignment if T is only copy constructible
    template <typename TT = T>
    void copy_assign(T const &t, std::enable_if_t<!std::is_copy_assignable<TT>::value>* = nullptr)
        noexcept(std::is_nothrow_copy_constructible<T>::value)
    {
        if (valid())
            reset();
        new (&value) T(t);
        m_valid = true;
    }

    // move assignment if T is move assignable
    template <typename TT = T>
    void move_assign(T &&t, std::enable_if_t<std::is_move_assignable<TT>::value>* = nullptr)
        noexcept(std::is_nothrow_move_constructible<T>::value && std::is_nothrow_move_assignable<T>::value)
    {
        if (valid()) {
            value = std::move(t);
        } else {
            new (&value) T(std::move(t));
            m_valid = true;
        }
    }

    // move assignment if T is only move constructible
    template <typename TT = T>
    void move_assign(T &&t, std::enable_if_t<!std::is_move_assignable<TT>::value>* = nullptr)
        noexcept(std::is_nothrow_move_constructible<T>::value)
    {
        if (valid())
            reset();
        new (&value) T(std::move(t));
        m_valid = true;
    }
protected:
    bool m_valid;
    union {
        empty_t empty;
        T value;
    };
};

template <typename T>
using optional_storage_parent = std::conditional_t<std::is_literal_type<T>::value,
                                                   trivial_optional_storage<T>,
                                                   nontrivial_optional_storage<T>
                                                  >;

template <typename T>
struct optional_storage : optional_storage_parent<T>
{
    using optional_storage_parent<T>::optional_storage_parent;
};


template <typename T>
class optional : public optional_storage<T>
{
    static_assert(!std::is_reference<T>::value, "Optional can not hold reference types. Maybe use std::reference_wrapper?");
    static_assert(!std::is_array<T>::value, "Optional can not hold array types.");
private:
    constexpr T& get_unchecked() noexcept {return optional_storage<T>::value;}
    constexpr const T& get_unchecked() const noexcept {return optional_storage<T>::value;}
public:
    using value_type = T;
    using optional_storage<T>::optional_storage;

    constexpr T&       operator*() &       { assert(valid()); return get_unchecked(); }
    constexpr const T& operator*() const & { assert(valid()); return get_unchecked(); }
    constexpr T&&      operator*() &&      { assert(valid()); return std::move(get_unchecked()); }

    constexpr T*       operator->()        { return &**this; }
    constexpr const T* operator->() const  { return &**this; }

    constexpr T move_out() {
        assert(valid());
        T t {std::move(get_unchecked())};
        reset();
        return t;
    }

    constexpr optional& operator=(nullopt_t) noexcept
    {
       if (valid())
           reset();
       return *this;
    }

    template <typename U>
    constexpr T value_or(U &&def) const & {return valid() ? get_unchecked() : static_cast<T>(std::forward<U>(def)); }

    template <typename U>
    constexpr T value_or(U &&def) && {return valid() ? std::move(get_unchecked()) : static_cast<T>(std::forward<U>(def)); }

    constexpr void reset() noexcept {
        optional_storage<T>::reset();
    }

    constexpr bool valid() const {
        return optional_storage<T>::valid();
    }
    explicit operator bool() const {return valid();}
private:
    optional_storage<T> m_data;
};


template <typename T>
struct is_optional : std::false_type {};
template <typename T>
struct is_optional<optional<T>> : std::true_type {};

template <typename T>
struct is_optional_cvr : is_optional<std::remove_cv_t<std::remove_reference_t<T>>> {};

template <typename T>
static constexpr bool is_optional_cvr_v = is_optional_cvr<T>::value;

template <typename Src, typename Dst>
struct copy_cvr_impl { using type = Dst&&; };

template <typename Src, typename Dst>
struct copy_cvr_impl<Src&, Dst> { using type = Dst&; };

template <typename Src, typename Dst>
struct copy_cvr_impl<const Src&, Dst> { using type = const Dst&; };

template <typename Src, typename Dst>
struct copy_cvr_impl<const Src&&, Dst> { using type = const Dst&&; };

template <typename Src, typename Dst>
struct copy_cvr_impl<Src&&, Dst> { using type = Dst&&; };

template <typename Src, typename Dst>
struct copy_cvr : copy_cvr_impl<Src, std::remove_cv_t<std::remove_reference_t<Dst>>> {};

template <typename Src, typename Dst>
using copy_cvr_t = typename copy_cvr<Src, Dst>::type;


//! monadic bind
//! (>>=) :: Optional a -> (a -> Optional b) -> Optional b
template <typename O, typename F,
    typename = std::enable_if_t<is_optional_cvr<O>::value>,
    typename _T = typename std::remove_reference_t<O>::value_type,
    typename T = copy_cvr_t<O, _T>,
    typename _Res = decltype(std::declval<F>()(std::declval<T>()))>
_Res operator>>=(O &&o, F &&f) {
    static_assert(is_optional<_Res>::value, "f must return optional<T>");
    return o.valid() ? f(std::forward<T>(*o)) : _Res{};
}

//! fmap
//! <$> :: Optional a -> (a -> b) -> Optional b

template <typename _Res, typename T, bool /*_Res is void*/>
struct fmap_impl;

template <typename _Res, typename T>
struct fmap_impl<_Res, T, false> {
    template <typename O, typename F>
    static inline _Res apply(O &&o, F &&f) {
        return o.valid() ? _Res(f(std::forward<T>(*o))) : _Res{};
    }
};
template <typename _Res, typename T>
struct fmap_impl<_Res, T, true> {
    template <typename O, typename F>
    static inline void apply(O &&o, F &&f) {
        if (o.valid())
            f(std::forward<T>(*o));
    }
};

template <typename O, typename F,
    typename = std::enable_if_t<is_optional_cvr<O>::value>,
    typename _T = typename std::remove_reference_t<O>::value_type,
    typename T = copy_cvr_t<O, _T>,
    typename _FRes = decltype(std::declval<F>()(std::declval<T>())),
    typename _Res = std::conditional_t<std::is_same<_FRes, void>::value, void, fl::optional<_FRes>>>
_Res operator%(O &&o, F &&f) {
    return fmap_impl<_Res, T, std::is_same<_Res, void>::value>::apply(std::forward<O>(o), std::forward<F>(f));
}

template <typename T>
constexpr optional<std::decay_t<T>> make_optional(T&& value) {
    return optional<std::decay_t<T>>(std::forward<T>(value));
}

} // namespace fl
