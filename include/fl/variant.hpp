#pragma once

#include "utils.hpp"

#include <type_traits>
#include <algorithm>
#include <limits>
#include <cstdint>
#include <cassert>
#include <utility>
#include <stdexcept>
#include <cstddef>   // size_t
#include <new>       // operator new

#ifdef __cpp_exceptions
# include <stdexcept>
#endif

namespace fl {

template <typename...Types>
class variant;

#ifdef __cpp_exceptions
struct bad_variant_access : std::runtime_error {
    using std::runtime_error::runtime_error;
};
#endif

// variant_alternative
template <std::size_t Index, typename T>
struct variant_alternative;

#if defined(__clang__)
# if __has_builtin(__type_pack_element)
#  define has_type_pack_element
# endif
#endif

#if defined(has_type_pack_element)
template <std::size_t Index, template <class...> class Var, typename ...Types>
struct variant_alternative<Index, Var<Types...>>
{
    static_assert(Index < sizeof...(Types), "Index out of range");
    using type = __type_pack_element<Index, Types...>;
};
#else
template <std::size_t Index, template <class...> class Var, typename First, typename...Types>
struct variant_alternative<Index, Var<First, Types...>>
    : variant_alternative<Index - 1, Var<Types...>>
{
    static_assert(Index < sizeof...(Types) + 1, "Index out of range");
};

template <typename First, template <class...> class Var, typename...Types>
struct variant_alternative<0, Var<First, Types...>>
{
    using type = First;
};
#endif

template <std::size_t Index, typename T>
using variant_alternative_t = typename variant_alternative<Index, T>::type;

template <std::size_t Index, typename T>
struct variant_alternative<Index, const T>
    : std::add_const<variant_alternative<Index, T>> {};

template <std::size_t Index, typename T>
struct variant_alternative<Index, volatile T>
    : std::add_volatile<variant_alternative<Index, T>> {};

template <std::size_t Index, typename T>
struct variant_alternative<Index, const volatile T>
    : std::add_cv<variant_alternative<Index, T>> {};

//------------------------------------------------------------------------------

namespace detail {

struct throw_bad_variant {
#ifdef __cpp_exceptions
    [[noreturn, gnu::cold, gnu::noinline, gnu::noclone]]
    void impl() {
        throw bad_variant_access("bad variant");
    }
#else
    [[noreturn, gnu::cold]]
    void impl() {
        //assert(!"bad variant");
        // the only way for this to happen is exception in copy constructor ...
        // and since exceptions are disabled, it's impossible
        unreachable();
    }
#endif
    template <typename R = void>
    [[noreturn, gnu::cold]]
    R operator()(std::size_t, R* = nullptr) {
        impl();
        unreachable();
    }
};

struct do_nothing {
    template <typename R = void>
    inline void operator()(std::size_t, R* = nullptr) { }
};

template <std::size_t N, typename...StorageTypes>
struct smallest_storage;

template <std::size_t N, typename T, typename...Next>
struct smallest_storage<N, T, Next...> {
    using type = typename std::conditional_t<N <= std::numeric_limits<T>::max(),
                                    identity<T>,
                                    smallest_storage<N, Next...>>::type;
};
template <std::size_t N>
struct smallest_storage<N> {
    // static_assert(false, "No type to hold N.");
};

template <std::size_t N>
using smallest_storage_t = typename smallest_storage<N, uint8_t, uint16_t, uint32_t, uint64_t>::type;

template <typename T, std::size_t Acc, typename... Types>
struct direct_type_impl {
    static constexpr std::size_t value = std::numeric_limits<std::size_t>::max();
};

template <typename T, std::size_t Acc, typename First, typename... Types>
struct direct_type_impl<T, Acc, First, Types...>
{
    static constexpr std::size_t value = std::conditional_t<
        std::is_same<T, First>::value,
        std::integral_constant<std::size_t, Acc>,
        direct_type_impl<T, Acc+1, Types...>
    >::value;
};

template <typename T, typename... Types>
struct direct_type : direct_type_impl<T, 0, Types...> {};


struct err_ambiguous_conversion {
    static constexpr std::size_t value = std::numeric_limits<std::size_t>::max();
};

template <typename T, std::size_t Acc, typename... Types>
struct convertible_type;

template <typename T, std::size_t Acc, typename First, typename... Types>
struct convertible_type<T, Acc, First, Types...>
{
    static constexpr std::size_t value = std::conditional_t<
        std::is_convertible<T, First>::value,
        std::conditional_t<
            disjunction<std::is_convertible<T, Types>...>::value,
            err_ambiguous_conversion,
            std::integral_constant<std::size_t, Acc>
        >,
        convertible_type<T, Acc+1, Types...>
    >::value;
};

template <typename T, std::size_t Acc>
struct convertible_type<T, Acc>
{
    // static_assert(false, "No convertible type.");
    static constexpr std::size_t value = std::numeric_limits<std::size_t>::max();
};

template <typename T, typename... Types>
struct value_traits
{
    using value_type = typename std::remove_const<typename std::remove_reference<T>::type>::type;
    static constexpr std::size_t invalid_index = std::numeric_limits<std::size_t>::max();
    using direct_type_idx_t = direct_type<value_type, Types...>;
    static constexpr bool is_direct = direct_type_idx_t::value != invalid_index;
    static constexpr std::size_t index = std::conditional_t<
                                            is_direct,
                                            direct_type_idx_t,
                                            convertible_type<T, 0, Types...>
                                        >::value;
    static constexpr bool is_valid = index != invalid_index;
    using target_type = typename variant_alternative<is_valid ? index : 0, variant<Types...>>::type;
};

template <typename...Types>
struct variant_union;

template <bool is_triv, typename...Types>
struct variant_union_base;

template <typename Head, typename...Tail>
struct variant_union_base<true, Head, Tail...>
{
    using head_type = Head;
    using tail_type = variant_union<Tail...>;
    using Self = variant_union<Head, Tail...>;

    union {
        head_type head;
        tail_type tail;
    };

    ~variant_union_base() = default;

    constexpr variant_union_base()
        noexcept(std::is_nothrow_default_constructible<head_type>::value)
        : head()
    {
        static_assert(std::is_default_constructible<head_type>::value, "First type in variant must be default constructible to allow default construction of variant.");
    }

    template <std::size_t idx>
    using idx_t = std::integral_constant<std::size_t, idx>;

     // direct construction
    template <std::size_t idx, typename...Args, typename = std::enable_if_t<(idx > 0)> >
    constexpr variant_union_base(idx_t<idx>, Args&&...args)
        noexcept(std::is_nothrow_constructible<tail_type, idx_t<idx-1>, Args...>::value)
        : tail(idx_t<idx-1>{}, std::forward<Args>(args)...)
    {
    }

    template <typename...Args>
    constexpr variant_union_base(idx_t<0>, Args&&...args)
        noexcept(std::is_nothrow_constructible<head_type, Args...>::value)
        : head(std::forward<Args>(args)...)
    {
    }

    constexpr variant_union_base(std::size_t idx, const Self &o)
        : variant_union_base(o)
    {
    }

    constexpr variant_union_base(std::size_t idx, Self &&o)
        : variant_union_base(std::move(o))
    {
    }

};

template <typename Head, typename...Tail>
struct variant_union_base<false, Head, Tail...>
{
    using head_type = Head;
    using tail_type = variant_union<Tail...>;
    using Self = variant_union<Head, Tail...>;

    union {
        head_type head;
        tail_type tail;
    };

    ~variant_union_base() {};

    variant_union_base()
         noexcept(std::is_nothrow_default_constructible<head_type>::value)
         : head()
    {
        static_assert(std::is_default_constructible<head_type>::value, "First type in variant must be default constructible to allow default construction of variant.");
    }

    template <std::size_t idx>
    using idx_t = std::integral_constant<std::size_t, idx>;

    // direct construction
    template <std::size_t idx, typename...Args, typename = std::enable_if_t<(idx > 0)> >
    variant_union_base(idx_t<idx>, Args&&...args)
        noexcept(std::is_nothrow_constructible<tail_type, idx_t<idx-1>, Args...>::value)
        : tail(idx_t<idx-1>{}, std::forward<Args>(args)...)
    {
    }

    template <typename...Args>
    variant_union_base(idx_t<0>, Args&&...args)
        noexcept(std::is_nothrow_constructible<head_type, Args...>::value)
        : head(std::forward<Args>(args)...)
    {
    }

    // copy
    variant_union_base(std::size_t idx, const Self &o)
    {
        compile_switch(
            idx,
            std::make_index_sequence<1+sizeof...(Tail)> {},
            [this,&o](auto i) {
                auto &tv = static_cast<Self*>(this)->get_impl(i);
                auto &ov = o.get_impl(i);
                using T = std::remove_reference_t<decltype(tv)>;
                new (&tv) T(ov);
            }, detail::throw_bad_variant{}
        );
    }

    // move
    variant_union_base(std::size_t idx, Self &&o)
    {
        compile_switch(
            idx,
            std::make_index_sequence<1+sizeof...(Tail)> {},
            [this,&o](auto i) {
                auto &tv = static_cast<Self*>(this)->get_impl(i);
                auto &ov = o.get_impl(i);
                using T = std::remove_reference_t<decltype(tv)>;
                new (&tv) T(std::move(ov));
            }, detail::throw_bad_variant{}
        );
    }
};

template <bool is_triv>
struct variant_union_base<is_triv>
{
    ~variant_union_base() = default;
};


template <typename...Types>
using variant_union_parent = variant_union_base<
    conjunction<std::is_trivially_destructible<Types>...>::value,
    Types...>;

template <typename...Types>
struct variant_union : variant_union_parent<Types...>
{
    using parent = variant_union_parent<Types...>;
    template <std::size_t idx> using idx_t = std::integral_constant<std::size_t, idx>;
    using Self = variant_union<Types...>;

    ~variant_union() = default;

    template <typename...Args>
    constexpr variant_union(Args&&...args)
        : parent(std::forward<Args>(args)...)
    {
    }

    template <
        typename T,
        typename Traits = detail::value_traits<T, Types...>,
        typename = std::enable_if_t<Traits::is_valid>>
    constexpr T& get() {
        return get_impl(std::integral_constant<std::size_t, Traits::index>{});
    }

    template <std::size_t idx, typename = std::enable_if_t<(idx > 0)>>
    constexpr auto& get_impl(std::integral_constant<std::size_t, idx>) {
        return this->tail.get_impl(std::integral_constant<std::size_t, idx-1>{});
    }
    constexpr auto& get_impl(std::integral_constant<std::size_t, 0>) {
        return this->head;
    }

    template <
        typename T,
        typename Traits = detail::value_traits<T, Types...>,
        typename = std::enable_if_t<Traits::is_valid>>
    constexpr const T& get() const {
        return get_impl(std::integral_constant<std::size_t, Traits::index>{});
    }

    template <std::size_t idx, typename = std::enable_if_t<(idx > 0)>>
    constexpr const auto& get_impl(std::integral_constant<std::size_t, idx>) const {
        return this->tail.get_impl(std::integral_constant<std::size_t, idx-1>{});
    }
    constexpr const auto& get_impl(std::integral_constant<std::size_t, 0>) const {
        return this->head;
    }



    // copy
    struct copy_impl {
        template <std::size_t idx>
        constexpr Self operator()(idx_t<idx> i) const {
            return Self(i, self.get_impl(i));
        }
        constexpr explicit copy_impl(const Self &self) : self(self) {}
        const Self &self;
    };

    constexpr Self copy(std::size_t idx) const
    {
        return compile_switch(
            idx,
            std::make_index_sequence<sizeof...(Types)> {},
            copy_impl(*this), // not a lambda - has to be constexpr
            detail::throw_bad_variant{}
        );
    }

    // move
    struct move_impl {
        template <std::size_t idx>
        constexpr Self operator()(idx_t<idx> i) const {
            return Self(i, std::move(self.get_impl(i)));
        }
        constexpr explicit move_impl(Self &&self) : self(std::move(self)) {}
        Self &&self;
    };
    constexpr Self move(std::size_t idx)
    {
        return compile_switch(
            idx,
            std::make_index_sequence<sizeof...(Types)> {},
            move_impl(std::move(*this)), // not a lambda - has to be constexpr
            detail::throw_bad_variant{}
        );
    }

    void destroy(std::size_t idx)
    {
        return compile_switch(
            idx,
            std::make_index_sequence<sizeof...(Types)> {},
            [this](auto i) {
                auto &v = this->get_impl(i);
                using T = std::remove_reference_t<decltype(v)>;
                v.~T();
            }, detail::do_nothing{}
        );
    }
};


} // namespace detail

template <typename...Types>
class variant_base
{
public:
    static_assert(sizeof...(Types) > 0, "Template parameter type list of variant can not be empty.");
    static_assert(!detail::disjunction<std::is_reference<Types>...>::value, "Variant can not hold reference types. Maybe use std::reference_wrapper?");
    static_assert(!detail::disjunction<std::is_array<Types>...>::value, "Variant can not hold array types.");

    using type_index_t = detail::smallest_storage_t<sizeof...(Types)+1>;
    static constexpr type_index_t invalid_index = std::numeric_limits<type_index_t>::max();

    using data_type = detail::variant_union<Types...>;
};

template <typename... Types>
class variant_triv : public variant_base<Types...>
{
public:
    using type_index_t = typename variant_base<Types...>::type_index_t;
    using data_type = typename variant_base<Types...>::data_type;
    using self = variant<Types...>;

    template <typename...Args>
    explicit constexpr variant_triv(type_index_t ti, Args&&...args) : type_index(ti), data(std::forward<Args>(args)...) {}
    ~variant_triv() noexcept = default;


    constexpr variant_triv(self const& old)
        : type_index(old.type_index)
        , data(old.data.copy(old.type_index))
    {
    }

    constexpr variant_triv(self&& old)
        noexcept(detail::conjunction<std::is_nothrow_move_constructible<Types>...>::value)
        : type_index(old.type_index)
        , data(old.data.move(old.type_index))
    {
    }
protected:
    type_index_t type_index;
    data_type data;
};

template <typename...Types>
class variant_nontriv : public variant_base<Types...>
{
public:
    using type_index_t = typename variant_base<Types...>::type_index_t;
    using data_type = typename variant_base<Types...>::data_type;
    using self = variant<Types...>;

    template <typename...Args>
    explicit constexpr variant_nontriv(type_index_t ti, Args&&...args) : type_index(ti), data(std::forward<Args>(args)...) {}
    ~variant_nontriv() noexcept {
        data.destroy(type_index);
    }

    variant_nontriv(self const& old)
        : type_index(old.type_index)
        , data(old.type_index, old.data)
    {
    }

    variant_nontriv(self&& old)
        noexcept(detail::conjunction<std::is_nothrow_move_constructible<Types>...>::value)
        : type_index(old.type_index)
        , data(old.type_index, std::move(old.data))
    {
    }
protected:
    type_index_t type_index;
    data_type data;
};

template <typename...Types>
using variant_parent = std::conditional_t<
    detail::conjunction<std::is_trivially_destructible<Types>...>::value,
    variant_triv<Types...>,
    variant_nontriv<Types...>>;

template <typename...Types>
class variant : public variant_parent<Types...>
{
    using parent = variant_parent<Types...>;
public:
    using type_index_t = typename parent::type_index_t;
    using data_type = typename parent::data_type;
    static constexpr type_index_t invalid_index = parent::invalid_index;
    using self = variant<Types...>;
public:
    template <typename T, typename = std::enable_if_t<detail::value_traits<T, Types...>::is_direct>>
    struct id {
        static constexpr std::size_t value = detail::value_traits<T, Types...>::index;
    };

    constexpr variant() noexcept(std::is_nothrow_constructible<parent,std::size_t>::value)
        : parent(0) // 0, {}
    {
    }

    template <typename T, typename Traits = detail::value_traits<T, Types...>,
              typename Enable = std::enable_if_t<Traits::is_valid && !std::is_same<self, typename Traits::value_type>::value>>
    constexpr variant(T&& val) noexcept(std::is_nothrow_constructible<typename Traits::target_type, T&&>::value)
        : parent(Traits::index,
                 std::integral_constant<std::size_t, Traits::index>{}, std::forward<T>(val))
    {
    }

    constexpr variant(self const& old)
        : parent(old)
    {
    }

    constexpr variant(self&& old)
        noexcept(detail::conjunction<std::is_nothrow_move_constructible<Types>...>::value)
        : parent(std::move(old))
    {
    }
private:
    void copy_assign(self const& rhs)
    {
        this->data.destroy(this->type_index);
        this->type_index = invalid_index;
        new (&this->data) data_type(rhs.type_index, rhs.data);
        this->type_index = rhs.type_index;
    }

    void move_assign(self&& rhs)
    {
        this->data.destroy(this->type_index);
        this->type_index = invalid_index;
        new (&this->data) data_type(rhs.type_index, std::move(rhs.data));
        this->type_index = rhs.type_index;
    }
public:
    self& operator=(self&& other)
        // note we check for nothrow-constructible, not nothrow-assignable, since
        // move_assign uses move-construction via placement new.
        noexcept(detail::conjunction<std::is_nothrow_move_constructible<Types>...>::value)
    {
        move_assign(std::move(other));
        return *this;
    }

    self& operator=(self const& other)
        noexcept(detail::conjunction<std::is_nothrow_copy_constructible<Types>...>::value)
    {
        copy_assign(other);
        return *this;
    }

    // conversions
    // copy/move-assign
    template <typename T, typename Traits = detail::value_traits<T, Types...>,
              typename Enable = std::enable_if_t<Traits::is_valid && !std::is_same<self, typename Traits::value_type>::value>>
    self& operator=(T&& rhs)
        // not that we check is_nothrow_constructible<T>, not is_nothrow_move_assignable<T>,
        // since we construct a temporary
        noexcept(std::is_nothrow_constructible<typename Traits::target_type, T&&>::value
                 && std::is_nothrow_move_assignable<self>::value)
    {
        self temp(std::forward<T>(rhs));
        move_assign(std::move(temp));
        return *this;
    }


    template <typename T, typename = std::enable_if_t<detail::value_traits<T, Types...>::is_direct>>
    constexpr bool is() const {
        return parent::type_index == detail::direct_type<T, Types...>::value;
    }

    constexpr bool valid() const {
        return this->type_index != invalid_index;
    }

    template <typename T, typename... Args>
    std::enable_if_t<detail::value_traits<T, Types...>::is_direct, void>
    emplace(Args&&... args)
    {
        this->data.destroy(this->type_index);
        this->type_index = invalid_index;
        new (&this->data.template get<T>()) T(std::forward<Args>(args)...);
        this->type_index = detail::direct_type<T, Types...>::value;
    }


    template <typename T, typename = std::enable_if_t<detail::value_traits<T, Types...>::is_direct>>
    constexpr T& get_unchecked() & {
        return this->data.template get<T>();
    }
    template <typename T, typename = std::enable_if_t<detail::value_traits<T, Types...>::is_direct>>
    const T& get_unchecked() const & {
        return *reinterpret_cast<T const*>(&this->data);
    }
    template <typename T, typename = std::enable_if_t<detail::value_traits<T, Types...>::is_direct>>
    constexpr T&& get_unchecked() && {
        return std::move(this->data.template get<T>());
    }

    constexpr type_index_t which() const noexcept {
        return this->type_index;
    }

    constexpr type_index_t index() const noexcept {
        return this->type_index;
    }

    constexpr type_index_t& __type() noexcept {
        return this->type_index;
    }

    // visitor
    template <typename F, typename V>
    [[gnu::always_inline]]
    constexpr auto static inline visit(V &&v, F&& f) {
        return compile_switch(
            v.which(),
            std::make_index_sequence<sizeof...(Types)> {},
            [&v,&f](auto i) {
                using T = variant_alternative_t<decltype(i)::value, variant<Types...>>;
                return f(std::forward<V>(v).template get_unchecked<T>());
            }, detail::throw_bad_variant{}
        );
    }

    // match
    template <typename... Fs>
    [[gnu::always_inline]]
    constexpr auto match(Fs&&... fs) const & {
        return visit(*this, detail::make_visitor(std::forward<Fs>(fs)...));
    }
    // non-const
    template <typename... Fs>
    [[gnu::always_inline]]
    constexpr auto match(Fs&&... fs) & {
        return visit(*this, detail::make_visitor(std::forward<Fs>(fs)...));
    }
    // move
    template <typename... Fs>
    [[gnu::always_inline]]
    constexpr auto match(Fs&&... fs) && {
        return visit(std::move(*this), detail::make_visitor(std::forward<Fs>(fs)...));
    }

    void destroy() {
        this->data.destroy(this->type_index);
    }
};


template <typename T>
struct is_variant : std::false_type {};
template <typename... Ts>
struct is_variant<variant<Ts...>> : std::true_type {};

template <typename T>
struct is_variant_cvr : is_variant<std::remove_cv_t<std::remove_reference_t<T>>> {};

template <typename V, typename F, typename = std::enable_if_t<is_variant_cvr<V>::value>>
[[gnu::always_inline]]
inline auto visit(F &&f, V &&v)
{
    return std::forward<V>(v).match(std::forward<F>(f));
}

} // namespace fl
