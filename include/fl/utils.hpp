#pragma once

#include <type_traits>
#include <algorithm>
#include <limits>
#include <cstdint>
#include <utility>
#include <cstddef>   // size_t

#ifdef __GNUC__
# define fl_likely(e)   __builtin_expect((bool)(e), true)
# define fl_unlikely(e) __builtin_expect((bool)(e), false)
#else
# define fl_likely(e)   (e)
# define fl_unlikely(e) (e)
#endif

namespace fl {

//------------------------------------------------------------------------------
//--- Compile time switch
//------------------------------------------------------------------------------

namespace detail {

template <typename R = int>
inline R unreachable()
{
#ifdef _MSC_VER
    __assume(false);
#elif defined(__GNUC__)
    __builtin_unreachable();
#else
    // not returning is UB - this function may not be actually called ...
#endif
}

template<class...> struct disjunction : std::false_type { };
template<class B1> struct disjunction<B1> : B1 { };
template<class B1, class... Bn>
struct disjunction<B1, Bn...>
    : std::conditional_t<bool(B1::value), B1, disjunction<Bn...>>  { };

template<class...> struct conjunction : std::true_type { };
template<class B1> struct conjunction<B1> : B1 { };
template<class B1, class... Bn>
struct conjunction<B1, Bn...>
    : std::conditional_t<bool(B1::value), conjunction<Bn...>, B1> {};

template <typename F>
struct on_scope_exit {
    explicit on_scope_exit(F &&f) : f(std::move(f)) {}
    ~on_scope_exit() {f();}
    on_scope_exit(on_scope_exit const &) = delete;
    on_scope_exit(on_scope_exit &&) = delete;
    on_scope_exit& operator=(on_scope_exit const &) = delete;
    on_scope_exit& operator=(on_scope_exit &&) = delete;
private:
    F f;
};

#define ON_SCOPE_EXIT(f) auto __f = f; on_scope_exit<decltype(__f)> __f_1 {std::move(__f)}

template <typename F, typename FD, std::size_t ... Is>
struct compile_switch_impl {
    using result_type = std::common_type_t<decltype(std::declval<F>()(std::integral_constant<std::size_t, Is>{}))...>;
    using R = result_type;

    template <typename G>
    struct FF {
        G &h;
        template <typename I, typename = std::enable_if_t<(I::value < sizeof...(Is))>>
        [[gnu::always_inline]]
        constexpr R operator()(I i) {
            return h(i);
        }

        [[gnu::always_inline]]
        R operator()(...) {
            return unreachable<R>();
        }
    };

    template <typename G, typename FDefault>
    [[gnu::always_inline]]
    static constexpr R apply (std::size_t i, std::index_sequence<Is...>, G &&h, FDefault &&fd)
    {
        if (fl_unlikely(i >= sizeof...(Is))) {
            return fd(i, static_cast<R*>(nullptr));
        }

        FF<G> f {h};
        #define CASE(N) case N: return f(std::integral_constant<std::size_t, N>{});
        #define CASE2(N) CASE(2*(N)) CASE(2*(N)+1)
        #define CASE4(N) CASE2(2*(N)) CASE2(2*(N)+1)
        #define CASE8(N) CASE4(2*(N)) CASE4(2*(N)+1)
        #define CASE16(N) CASE8(2*(N)) CASE8(2*(N)+1)
        #define CASE32(N) CASE16(2*(N)) CASE16(2*(N)+1)
        #define CASE64(N) CASE32(2*(N)) CASE32(2*(N)+1)
        constexpr std::size_t MAX_CASES = 64;
        static_assert(sizeof...(Is) <= MAX_CASES, "Implementation limited to 64 cases.");
        switch (i) {
            CASE64(0)
        }
        return unreachable<R>();
        #undef CASE64
        #undef CASE32
        #undef CASE16
        #undef CASE8
        #undef CASE4
        #undef CASE2
        #undef CASE
    }
};

template <typename T>
struct identity {
    using type = T;
};

template <typename... Fns>
struct visitor;

template <typename Fn>
struct visitor<Fn> : Fn
{
    using Fn::operator();

    template<typename T>
    visitor(T&& fn) : Fn(std::forward<T>(fn)) {}
};

template <typename Fn, typename... Fns>
struct visitor<Fn, Fns...> : Fn, visitor<Fns...>
{
    using Fn::operator();
    using visitor<Fns...>::operator();

    template<typename T, typename... Ts>
    constexpr visitor(T&& fn, Ts&&... fns)
        : Fn(std::forward<T>(fn))
        , visitor<Fns...>(std::forward<Ts>(fns)...) {}
};

template <typename... Fns>
inline constexpr detail::visitor<typename std::decay<Fns>::type...> make_visitor(Fns&&... fns)
{
    return detail::visitor<typename std::decay<Fns>::type...>
        (std::forward<Fns>(fns)...);
}
} // namespace detail

template <typename F, typename FD, std::size_t ... Is>
[[gnu::always_inline]]
inline constexpr auto compile_switch(
        std::size_t i, std::index_sequence<Is...> seq, F &&h, FD &&fd)
-> typename detail::compile_switch_impl<F, FD, Is...>::result_type
{
    return detail::compile_switch_impl<F, FD, Is...>::apply(i, seq, std::forward<F>(h), std::forward<FD>(fd));
}


}
