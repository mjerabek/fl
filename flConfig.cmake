get_filename_component(fl_CMAKE_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
include(CMakeFindDependencyMacro)

# find_dependency(XXX version REQUIRED MODULE)

if(NOT TARGET fl)
    include("${fl_CMAKE_DIR}/flTargets.cmake")
endif()
set(fl_LIBRARIES fl)
