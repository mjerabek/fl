# Functional Library

NOTE: The code works, is used in production, but lacks tests for corner-cases or subtleties.
That's still TBD.

## What is this?

C++14 implementation of `optional`, `variant`, and `Result`.

Features:
 * constexpr-enabled
 * variant visitation produces jump table (switch statement), not a vtable (as `std::variant` does)
 * space-optimization for `optional`, where the underlying type has an invalid value (nullptr, NaN)
 * operates both with exceptions enabled and disabled
 * suitable for embedded

The interface of `optional` and `variant` should be mostly the same as of their `std` variants (or mapbox::variant).
`Result` is an analogue of Rust's `Result` (which itself is an analogue of Haskell's `Either`).

## Why not use ...

`std::optional`, `std::variant`:
 * visitation uses vtables instead of jump tables

mapbox::variant:
 * no support for constexpr

And of course, none of them support monadic operations, though they can be added easily.
`boost::variant` might support all of the above, but I did not want to introduce dependency on boost to my projects.
As an aside, writing this was an interesting exercise.



## Credits

Parts of the implementation are inspired or directly taken from [mapbox/variant](https://github.com/mapbox/variant),
licensed under BSD license.

## License

MIT
